<section id="content-edit">
	<div class="container-sm" >
		<div class="card" id="card-edit">
			<h2> <?php echo $title; ?> </h2>
			<!-- Formulario  nuevo cliente -->
			<div class="card-body" id="form-edit">
				<?php echo validation_errors(); ?>

				<?php echo form_open('cliente/crear'); ?>

				<div class="row g-3 align-items-center in-edit">
					<div class="col-auto">
						<label for="inputPassword6" class="col-form-label">Nombre</label>
					</div>
					<div class="col-auto">
						<input type="text" name="nombre" id="inputPassword6" class="form-control" aria-describedby="passwordHelpInline"   >
					</div>
				</div>
				<div class="row g-3 align-items-center in-edit">
					<div class="col-auto">
						<label for="inputPassword6" class="col-form-label">Apellido</label>
					</div>
					<div class="col-auto">
						<input type="text" name="apellido" id="inputPassword6" class="form-control" aria-describedby="passwordHelpInline"   >
					</div>
				</div>
				<div class="row g-3 align-items-center in-edit">
					<div class="col-auto">
						<label for="inputPassword6" class="col-form-label">Teléfono</label>
					</div>
					<div class="col-auto">
						<input type="text" name="telefono" id="inputPassword6" class="form-control" aria-describedby="passwordHelpInline"   >
					</div>
				</div>
				<div class="row g-3 align-items-center in-edit">
					<div class="col-auto">
						<label for="inputPassword6" class="col-form-label">Correo</label>
					</div>
					<div class="col-auto">
						<input type="email" name="correo" id="inputPassword6" class="form-control" aria-describedby="passwordHelpInline"   >
					</div>
				</div>

					<input type="submit" name="submit" class="btn btn-outline-success" value="Nuevo Cliente" />
				</form>
			</div>
		</div>
	</div>
</section>
