<!-- Se muestra la lista de todos los clientes-->
	<section >
		<div class="container-sm">

			<div class="card-body">
				<h1>Clientes</h1>

				<div class="btn-nuevo">
					<a class=" btn btn-outline-info" href="<?php echo site_url('cliente/crear'); ?>">Nueva Cuenta</a>
				</div>
				<h2><?php echo $title; ?></h2>

				<table class="table">
				<thead>
					<tr>
					<th>Nombre</th>
					<th>Apellido</th>
					<th>Teléfono</th>
					<th>Correo</th>
					<th>Acciones</th>
					</tr>
				</thead>
				<tbody>
				<?php foreach ($clientes as $cliente): ?>

				<tr>
					<td> <?php echo $cliente['nombre']; ?></td>
					<td> <?php echo $cliente['apellido']; ?></td>
					<td> <?php echo $cliente['telefono']; ?></td>
					<td> <?php echo $cliente['correo']; ?></td>
					<td> <a href="<?php echo site_url('cliente/editar/'.$cliente['id']); ?>"
							class="btn btn-outline-secondary" >Editar</a>
						<a href="<?php echo site_url('cliente/eliminar/'.$cliente['id']); ?>" class="btn btn-outline-danger">Eliminar</a> <td>
				</tr>
				<?php endforeach; ?>
				</tbody>
				</table>
			</div>
    	</div>
	</section>
