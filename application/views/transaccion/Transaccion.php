<!-- Se muestra la lista de todas las transacciones-->
<section >
	<div class="container-sm">

		<div class="card-body">
			<h1>Tipos de Transacciones </h1>
			<div class="btn-nuevo">
				<a class=" btn btn-outline-info" href="<?php echo site_url('transaccion/crear'); ?>">Nuevo Tipo de Transacción</a>
			</div>
			<h2><?php echo $title; ?></h2>

			<table class="table">
				<thead>
				<tr>
					<th>Nro</th>
					<th>Cuenta Bancaria</th>
					<th>Tipo Transacción</th>
					<th>Monto</th>
				</tr>
				</thead>
				<tbody>
				<?php foreach ($transacciones as $transaccion): ?>
					<tr>
						<td> <?php echo $transaccion['id']; ?></td>
						<td> <?php echo $transaccion['id_cuenta_bancaria']; ?></td>
						<td> <?php echo $transaccion['id_tipo_transaccion']; ?></td>
						<td> <?php echo $transaccion['monto']; ?></td>
						<td> <a href="<?php echo site_url('transaccion/editar/'.$transaccion['id']); ?>"
								class="btn btn-outline-secondary" >Editar</a>
							<a href="<?php echo site_url('transaccion/eliminar/'.$transaccion['id']); ?>" class="btn btn-outline-danger">Eliminar</a> <td>
					</tr>
				<?php endforeach; ?>
				</tbody>
			</table>
		</div>
	</div>
</section>
