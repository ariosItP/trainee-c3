<!-- Se muestra la lista de todas las transacciones-->
<section >
	<div class="container-sm">

		<div class="card-body">
			<h1>Tipos de Transacciones </h1>
			<div class="btn-nuevo">
				<a class=" btn btn-outline-info" href="<?php echo site_url('transaccion/crear'); ?>">Nuevo Tipo de Transacción</a>
			</div>
			<h2><?php echo $title; ?></h2>

			<table class="table">
				<thead>
				<tr>
					<th>Nro</th>
					<th>Nombre</th>
					<th>Descripción</th>
				</tr>
				</thead>
				<tbody>
				<?php foreach ($tipo_transaccion as $tipo): ?>
					<tr>
						<td> <?php echo $tipo['id']; ?></td>
						<td> <?php echo $tipo['nombre']; ?></td>
						<td> <?php echo $tipo['descripcion']; ?></td>
						<td> <a href="<?php echo site_url('tipoTransaccion/editar/'.$tipo['id']); ?>"
								class="btn btn-outline-secondary" >Editar</a>
							<a href="<?php echo site_url('tipoTransaccion/eliminar/'.$tipo['id']); ?>" class="btn btn-outline-danger">Eliminar</a> <td>
					</tr>
				<?php endforeach; ?>
				</tbody>
			</table>
		</div>
	</div>
</section>
