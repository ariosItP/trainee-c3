<section id="content-edit">
	<div class="container-sm" >
		<div class="card" id="card-edit">
			<h2> <?php echo $title; ?> </h2>
			<!-- Formulario  editar tipo de transaccion -->
			<div class="card-body" id="form-edit">
				<?php echo validation_errors(); ?>

				<?php echo form_open('tipoTransaccion/modificar/'. $tipo_transaccion->id); ?>

				<div class="row g-3 align-items-center in-edit">
					<div class="col-auto">
						<label for="inputPassword6" class="col-form-label">Nombre</label>
					</div>
					<div class="col-auto">
						<input type="text" name="nombre" id="inputPassword6" class="form-control" aria-describedby="passwordHelpInline"  value="<?php echo $tipo_transaccion->nombre ; ?>"  >
					</div>
				</div>
				<div class="row g-3 align-items-center in-edit">
					<div class="col-auto">
						<label for="inputPassword6" class="col-form-label">Descripción</label>
					</div>
					<div class="col-auto">
						<input type="text" name="descripcion" id="inputPassword6" class="form-control" aria-describedby="passwordHelpInline"  value="<?php echo $tipo_transaccion->descripcion ; ?>"   >
					</div>
				</div>
				<input type="submit" name="submit" class="btn btn-outline-success" value="Nuevo Tipo de Transacción" />
				</form>
			</div>
		</div>
	</div>
</section>
