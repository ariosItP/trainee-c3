<section id="content-edit">
	<div class="container-sm" >
		<div class="card" id="card-edit">
			<h2> <?php echo $title; ?> </h2>
			<!-- Formulario  editar tipo de cuenta -->
			<div class="card-body" id="form-edit">
				<?php echo validation_errors(); ?>

				<?php echo form_open('cuentaBancaria/modificar/'. $cuenta->id); ?>

				<div class="row g-3 align-items-center in-edit">
					<div class="col-auto">
						<label for="inputPassword6" class="col-form-label">Moneda</label>
					</div>
					<div class="col-auto">
						<input type="text" name="moneda" id="inputPassword6" class="form-control" aria-describedby="passwordHelpInline"  value="<?php echo $cuenta->moneda ; ?>" >
					</div>
				</div>
				<div class="row g-3 align-items-center in-edit">
					<div class="col-auto">
						<label for="inputPassword6" class="col-form-label">Codigo de Clientes</label>
					</div>
					<div class="col-auto">
						<input type="text" name="codigo_cliente" id="inputPassword6" class="form-control" aria-describedby="passwordHelpInline"  value="<?php echo $cuenta->codigo_cliente ; ?>" >
					</div>
				</div>

				<input type="submit" name="submit" class="btn btn-outline-success" value="Editar Cuenta" />
				</form>
			</div>
		</div>
	</div>
</section>
