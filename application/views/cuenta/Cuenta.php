<!-- Se muestra la lista de todas las cuentas-->
<section >
	<div class="container-sm">

		<div class="card-body">
			<h1>Cuentas Bancarias</h1>
			<div class="btn-nuevo">
				<a class=" btn btn-outline-info" href="<?php echo site_url('cuenta/crear'); ?>">Nueva Cuenta</a>
			</div>
			<h2><?php echo $title; ?></h2>

			<table class="table">
				<thead>
				<tr>
					<th>Nro</th>
					<th>Moneda</th>
					<th>Cliente</th>
					<th>Acciones</th>
				</tr>
				</thead>
				<tbody>
				<?php foreach ($cuentas as $cuenta): ?>

					<tr>
						<td> <?php echo $cuenta['id']; ?></td>
						<td> <?php echo $cuenta['moneda']; ?></td>
						<td> <?php echo $cuenta['codigo_cliente']; ?></td>
						<td> <a href="<?php echo site_url('cuentaBancaria/editar/'.$cuenta['id']); ?>"
								class="btn btn-outline-secondary" >Editar</a>
							<a href="<?php echo site_url('cuentaBancaria/eliminar/'.$cuenta['id']); ?>" class="btn btn-outline-danger">Eliminar</a> <td>
					</tr>
				<?php endforeach; ?>
				</tbody>
			</table>
		</div>
	</div>
</section>
