<?php
class clienteModel extends CI_Model {

    public function __construct()
    {
        parent:: __construct();
    }
    
    /**
     * consigue de la base de datos los resultados de la tabla clientes
     */
    public function getClientes()
    {  
     
        $query = $this->db->get('clientes');
        return $query->result_array();
    
    }

    /**
     * se inserta datos del nuevo cliente registrado en la tabla Clientes
     */
    public function crearCliente()
    {
        $this->load->helper('url');

        $data = array(
            'nombre' => $this->input->post('nombre'),
            'apellido' => $this->input->post('apellido'),
            'telefono' => $this->input->post('telefono'),
            'correo' => $this->input->post('correo'),
        );

        return $this->db->insert('clientes', $data);
    }

    
    /**
     * Pre:consigue datos de un cliente con  id= $id
     * Post: retorna los datos del cliente con id=$id
     */
    public function getCliente($id)
    {
        $this->load->helper('url');
       $query = $this->db->get_where('clientes', array('id' => $id));
	   return $query->row();
        
    }

	/**
	 * Pre: consigue datos de un cliente con id= $id
	 * Post: actualiza datos del cliente con id=$id
	 */
	public function updateCliente($id)
	{
		$this->load->helper('url');

		$data = array(
			'nombre' => $this->input->post('nombre'),
			'apellido' => $this->input->post('apellido'),
			'telefono' => $this->input->post('telefono'),
			'correo' => $this->input->post('correo'),
		);

		$this->db->where('id', $id);
		$this->db->update('clientes', $data);

	}


	/**
	 *
	 * Pre: consigue datos de un cliente con id= $id
	 * Post: elimina el registro del cliente con id=$id
	 */
	public function deleteCliente($id)
	{
		$this->load->helper('url');

		$this->db->where('id', $id);
		$this->db->delete('clientes');

	}
}
