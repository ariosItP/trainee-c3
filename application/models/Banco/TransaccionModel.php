<?php
class transaccionModel extends CI_Model {

	public function __construct()
	{
		parent:: __construct();
	}

	/**
	 * consigue de la base de datos los resultados de la tabla 'transacciones'
	 */
	public function getTransacciones()
	{

		$query = $this->db->get('transacciones');
		return $query->result_array();

	}

	/**
	 * se inserta datos una nueva cuenta registrada en la tabla 'transacciones'
	 */
	public function crearTransaccion()
	{
		$this->load->helper('url');

		$data = array(
			'id_cuenta_bancaria' => $this->input->post('id_cuenta_bancaria'),
			'id_tipo_transaccion' => $this->input->post('id_tipo_transaccion'),
			'monto' => $this->input->post('monto'),

		);

		return $this->db->insert('transacciones', $data);
	}


	/**
	 * Pre:consigue datos de una transaccion con  id= $id
	 * Post: retorna los datos de una transaccion con id=$id
	 */
	public function getTransaccion($id)
	{
		$this->load->helper('url');
		$query = $this->db->get_where('transacciones', array('id' => $id));
		return $query->row();

	}

	/**
	 * Pre: consigue datos de una transaccion con id= $id
	 * Post: actualiza datos de una transaccion con id=$id
	 */
	public function updateTransaccion($id)
	{
		$this->load->helper('url');

		$data = array(
			'id_cuenta_bancaria' => $this->input->post('id_cuenta_bancaria'),
			'id_tipo_transaccion' => $this->input->post('id_tipo_transaccion'),
			'monto' => $this->input->post('monto'),

		);

		$this->db->where('id', $id);
		$this->db->update('transacciones', $data);
	}

	/**
	 *
	 * Pre: consigue datos de una transaccion con id= $id
	 * Post: elimina el registro de una transaccion con id=$id
	 */
	public function deleteTransaccion($id)
	{
		$this->load->helper('url');

		$this->db->where('id', $id);
		$this->db->delete('transacciones');

	}
}
