<?php
class cuentaBancariaModel extends CI_Model {

	public function __construct()
	{
		parent:: __construct();
	}

	/**
	 * consigue de la base de datos los resultados de la tabla 'cuentas_bancarias'
	 */
	public function getCuentasBancarias()
	{

		$query = $this->db->get('cuentas_bancarias');
		return $query->result_array();

	}

	/**
	 * se inserta datos una nueva cuenta registrada en la tabla 'cuentas_bancarias'
	 */
	public function crearCuentaBancaria()
	{
		$this->load->helper('url');

		$data = array(
			'moneda' => $this->input->post('moneda'),
			'codigo_cliente' => $this->input->post('codigo_cliente'),

		);

		return $this->db->insert('cuentas_bancarias', $data);
	}


	/**
	 * Pre:consigue datos de una cuenta con  id= $id
	 * Post: retorna los datos de cuenta con id=$id
	 */
	public function getCuentaBancaria($id)
	{
		$this->load->helper('url');
		$query = $this->db->get_where('cuentas_bancarias', array('id' => $id));
		return $query->row();

	}

	/**
	 * Pre: consigue datos de una cuenta con id= $id
	 * Post: actualiza datos de cuenta con id=$id
	 */
	public function updateCuentaBancaria($id)
	{
		$this->load->helper('url');

		$data = array(
			'moneda' => $this->input->post('moneda'),
			'codigo_cliente' => $this->input->post('codigo_cliente'),
		);

		$this->db->where('id', $id);
		$this->db->update('cuentas_bancarias', $data);
	}

	/**
	 *
	 * Pre: consigue datos de una cuenta con id= $id
	 * Post: elimina el registro de cuenta con id=$id
	 */
	public function deleteCuentaBancaria($id)
	{
		$this->load->helper('url');

		$this->db->where('id', $id);
		$this->db->delete('cuentas_bancarias');

	}
}
