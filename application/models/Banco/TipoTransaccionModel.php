<?php
class tipoTransaccionModel extends CI_Model {

	public function __construct()
	{
		parent:: __construct();
	}

	/**
	 * consigue de la base de datos los resultados de la tabla 'tipo_transaccion'
	 */
	public function getTipoTransacciones()
	{

		$query = $this->db->get('tipo_transaccion');
		return $query->result_array();

	}

	/**
	 * se inserta datos una nueva cuenta registrada en la tabla 'tipo_transaccion'
	 */
	public function crearTipoTransaccion()
	{
		$this->load->helper('url');

		$data = array(
			'nombre' => $this->input->post('nombre'),
			'descripcion' => $this->input->post('descripcion'),

		);

		return $this->db->insert('tipo_transaccion', $data);
	}


	/**
	 * Pre:consigue datos de un tipo_transaccion con  id= $id
	 * Post: retorna los datos de un tipo_transaccion con id=$id
	 */
	public function getTipoTransaccion($id)
	{
		$this->load->helper('url');
		$query = $this->db->get_where('tipo_transaccion', array('id' => $id));
		return $query->row();

	}

	/**
	 * Pre: consigue datos de un tipo_transaccion con id= $id
	 * Post: actualiza datos de un tipo_transaccion con id=$id
	 */
	public function updateTipoTransaccion($id)
	{
		$this->load->helper('url');

		$data = array(
			'nombre' => $this->input->post('nombre'),
			'descripcion' => $this->input->post('descripcion'),
		);

		$this->db->where('id', $id);
		$this->db->update('tipo_transaccion', $data);
	}

	/**
	 *
	 * Pre: consigue datos de un tipo_transaccion con id= $id
	 * Post: elimina el registro de un tipo_transaccion con id=$id
	 */
	public function deleteTipoTransaccion($id)
	{
		$this->load->helper('url');

		$this->db->where('id', $id);
		$this->db->delete('tipo_transaccion');

	}
}
