<?php
class TipoTransaccion extends CI_Controller {

    public function __construct()
    {
            parent::__construct();
          
            $this->load->model('Banco/tipoTransaccionModel');
            $this->load->helper('url_helper');
    }

	/**
	 * envia resultados de los registros de la tabla tipo de Transaccion
	 * a la vista TipoTransaccion
	 */
	public function index()
	{
		$data['tipo_transaccion'] = $this->tipoTransaccionModel->getTipoTransacciones();
		$data['title'] = 'Lista de Tipos de Transacción';

		$this->load->view('templates/header');
		$this->load->view('tipoTransaccion/tipoTransaccion', $data);
		$this->load->view('templates/footer');
	}

	/**
	 * Se inserta el registro de un Tipo Transaccion en la tabla 'tipo_ransaccion'
	 * campos requeridos nombre, descripcion
	 * retorna a la vista TipoTransaccion (Lista de Tipos Transacción)
	 */
	public function crear()
	{
		$this->load->helper('form');
		$this->load->library('form_validation');

		$data['title'] = 'Nuevo Tipo de Transacción';

		$this->form_validation->set_rules('nombre','Nombre','required');
		$this->form_validation->set_rules('descripcion','Descripción','required');


		if ($this->form_validation->run() === FALSE)
		{
			$this->load->view('templates/header');
			$this->load->view('tipoTransaccion/nuevo',$data);
			$this->load->view('templates/footer');
		}
		else
		{
			$this->tipoTransaccionModel->crearTipoTransaccion();
			redirect('tipo_transaccion', 'index');
		}
	}

	/**
	 * Permite al usuario ver datos del tipo de transaccion por id
	 * para poder editar los datos
	 * @param $id
	 * @return muestra la vista editar con datos del tipo de transaccion
	 */
	public function editar( $id = null)
	{

		$this->load->helper('form');
		$this->load->library('form_validation');

		$data['tipo_transaccion']= $this->tipoTransaccionModel->getTipoTransaccion($id);

		$data['title'] = 'Editar Tipo de Transacción';

		$this->load->view('templates/header');
		$this->load->view('tipoTransaccion/editar',$data);
		$this->load->view('templates/footer');

	}


	/**
	 * Modifica datos del tipo de transcción
	 * @param $id
	 * @return a la vista tipoTransaccion (lista de tipos de Transacciones )
	 */
	public function modificar($id)
	{
		$this->load->helper('form');
		$this->load->library('form_validation');
		$data['title'] = 'Editar Tipo de Transacción';

		$this->form_validation->set_rules('nombre','Nombre', 'required');
		$this->form_validation->set_rules('descripcion','Descripción',  'required');

		$data['tipo_transaccion'] = $this->tipoTransaccionModel->getTipoTransacciones();

		if ($this->form_validation->run() === FALSE)
		{
			$this->load->view('templates/header');
			$this->load->view('tipoTransaccion/editar',$data);
			$this->load->view('templates/footer');
		}
		else
		{
			$this->tipoTransaccionModel->updateTipoTransaccion($id);
			redirect('tipo_transaccion', 'index');
		}
	}

	/**
	 * Elimina registro del tipo de transacción con id = $id
	 * @param $id
	 * @return a la vista tipoTaransaccion (Lista de tipos de transacción)
	 */
	public function eliminar( $id = null)
	{
		$this->load->helper('form');
		$this->load->library('form_validation');

		$this->tipoTransaccionModel->deleteTipoTransaccion($id);

		redirect('tipo_transaccion', 'index');

	}

}
