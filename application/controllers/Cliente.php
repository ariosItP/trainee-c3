<?php
class Cliente extends CI_Controller {

    /**
     * constructor, carga el modelo a usar y el helper
     */
    public function __construct()
    {
            parent::__construct();
          
            $this->load->model('Banco/clienteModel');
            $this->load->helper('url_helper');
    }

    /**
     * muestra en la vista cliente la lista de todos los clientes 
     * ruta :http://localhost/trainee-c3/Cliente/Cliente
     */
    public function index()
    { 
        $data['clientes'] = $this->clienteModel->getClientes();
        $data['title'] = 'Lista de Clientes';

        $this->load->view('templates/header');
        $this->load->view('cliente/cliente', $data);
        $this->load->view('templates/footer');
    }

    /**
     * Se inserta un nuevo cliente a la tabla Clientes
     * campos requeridos nombre, apellido, telefono y correo
     */
    public function crear()
    {
        $this->load->helper('form');
        $this->load->library('form_validation');

        $data['title'] = 'Nuevo Cliente';
       
        $this->form_validation->set_rules('nombre','Nombre', 'required');
        $this->form_validation->set_rules('apellido','Apellido',  'required');
        $this->form_validation->set_rules('telefono','Telefono', 'required');
        $this->form_validation->set_rules('correo','Correo', 'required');

        
        if ($this->form_validation->run() === FALSE)
        {
            $this->load->view('templates/header');
            $this->load->view('cliente/nuevo',$data);
            $this->load->view('templates/footer');
        }
        else
        {
            $this->clienteModel->crearCliente();
			redirect('/', 'index');
        }
    }

	/**
	 * Permite al usuario ver datos del cliente por id
	 * para poder editar los datos
	 * @param $id
	 * @return muestra la vista editar con datos del cliente
	 */
    public function editar( $id = null)
    {
        
        $this->load->helper('form');
        $this->load->library('form_validation');

        $data['cliente']= $this->clienteModel->getCliente($id);

        $data['title'] = 'Editar Cliente';
       
        $this->load->view('templates/header');
        $this->load->view('cliente/editar',$data);
        $this->load->view('templates/footer');
      
    }

	/**
	 * Modifica datos del cliente
	 * @param $id
	 * @return a la vista de lista de clientes
	 */
    public function modificar($id)
    {

        $this->load->helper('form');
        $this->load->library('form_validation');
		$data['title'] = 'Editar Cliente';

		$this->form_validation->set_rules('nombre','Nombre', 'required');
		$this->form_validation->set_rules('apellido','Apellido',  'required');
		$this->form_validation->set_rules('telefono','Telefono', 'required');
		$this->form_validation->set_rules('correo','Correo', 'required');
		$data['clientes'] = $this->clienteModel->getClientes();

		if ($this->form_validation->run() === FALSE)
		{
			$this->load->view('templates/header');
			$this->load->view('cliente/editar',$data);
			$this->load->view('templates/footer');
		}
		else
		{
			$this->clienteModel->updateCliente($id);
			redirect('/', 'index');
		}
    }

	/**
	 * Elimina registro de cliente por id
	 * @param $id
	 * @return a la lista de clientes
	 */
	public function eliminar( $id = null)
	{
		$this->load->helper('form');
		$this->load->library('form_validation');

		//$data['cliente']= $this->clienteModel->getCliente($id);

		$this->clienteModel->deleteCliente($id);

		redirect('/', 'index');

	}

}
