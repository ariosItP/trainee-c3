<?php
class CuentaBancaria extends CI_Controller {

    public function __construct()
    {
            parent::__construct();
          
            $this->load->model('Banco/cuentaBancariaModel');
            $this->load->helper('url_helper');
    }

	/**
	 * envia resultados de los registros de las cuentas_bancarias
	 * a la vista cuenta
	 */
	public function index()
	{
		$data['cuentas'] = $this->cuentaBancariaModel->getCuentasBancarias();
		$data['title'] = 'Lista de Cuentas Bancarias';

		$this->load->view('templates/header');
		$this->load->view('cuenta/cuenta', $data);
		$this->load->view('templates/footer');
	}

	/**
	 * Se inserta el registro de una nueva cuenta bancaria en la tabla 'cuentas_bancarias'
	 * campos requeridos moneda, codigo_cliente
	 * retorna a la vista cuentas (Lista de cuentas)
	 */
	public function crear()
	{
		$this->load->helper('form');
		$this->load->library('form_validation');

		$data['title'] = 'Nuevo Cuenta';

		$this->form_validation->set_rules('moneda','Moneda', 'required');
		$this->form_validation->set_rules('codigo_cliente','Cliente',  'required');


		if ($this->form_validation->run() === FALSE)
		{
			$this->load->view('templates/header');
			$this->load->view('cuenta/nuevo',$data);
			$this->load->view('templates/footer');
		}
		else
		{
			$this->cuentaBancariaModel->crearCuentaBancaria();
			redirect('cuentas', 'index');
		}
	}

	/**
	 * Permite al usuario ver datos de cuenta bancaria por id
	 * para poder editar los datos
	 * @param $id
	 * @return muestra la vista editar con datos de la cuenta
	 */
	public function editar( $id = null)
	{

		$this->load->helper('form');
		$this->load->library('form_validation');

		$data['cuenta']= $this->cuentaBancariaModel->getCuentaBancaria($id);

		$data['title'] = 'Editar Cuenta Bancaria';

		$this->load->view('templates/header');
		$this->load->view('cuenta/editar',$data);
		$this->load->view('templates/footer');

	}


	/**
	 * Modifica datos de la cuenta bancaria
	 * @param $id
	 * @return a la vista cuenta (lista de cuentas )
	 */
	public function modificar($id)
	{

		$this->load->helper('form');
		$this->load->library('form_validation');
		$data['title'] = 'Editar Cuenta Bancaria';

		$this->form_validation->set_rules('moneda','Moneda', 'required');
		$this->form_validation->set_rules('codigo_cliente','Cliente',  'required');

		$data['cuentas'] = $this->cuentaBancariaModel->getCuentasBancarias();

		if ($this->form_validation->run() === FALSE)
		{
			$this->load->view('templates/header');
			$this->load->view('cuenta/editar',$data);
			$this->load->view('templates/footer');
		}
		else
		{
			$this->cuentaBancariaModel->updateCuentaBancaria($id);
			redirect('cuentas', 'index');
		}
	}

	/**
	 * Elimina registro de la cuenta con id = $id
	 * @param $id
	 * @return a la vista cuentas (Lista de cuentas)
	 */
	public function eliminar( $id = null)
	{
		$this->load->helper('form');
		$this->load->library('form_validation');

		$this->cuentaBancariaModel->deleteCuentaBancaria($id);

		redirect('cuentas', 'index');

	}



}
