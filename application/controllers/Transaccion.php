<?php
class Transaccion extends CI_Controller {

	public function __construct()
	{
		parent::__construct();

		$this->load->model('Banco/transaccionModel');
		$this->load->helper('url_helper');
	}

	/**
	 * envia resultados de los registros de la tabla 'transaccion'
	 * a la vista Transaccion
	 */
	public function index()
	{
		$data['transacciones'] = $this->transaccionModel->getTransacciones();
		$data['title'] = 'Lista de Transacciones';

		$this->load->view('templates/header');
		$this->load->view('transaccion/transaccion', $data);
		$this->load->view('templates/footer');
	}

	/**
	 * Se inserta el registro de una Transaccion en la tabla 'transaccion'
	 * campos requeridos nombre, descripcion
	 * retorna a la vista Transaccion (Lista de Transacción)
	 */
	public function crear()
	{
		$this->load->helper('form');
		$this->load->library('form_validation');

		$data['title'] = 'Nueva Transacción';

		$this->form_validation->set_rules('id_cuenta_bancaria','Cuenta Bancaria','required');
		$this->form_validation->set_rules('id_tipo_transaccion','Tipo de transación','required');
		$this->form_validation->set_rules('monto','Monto','required');


		if ($this->form_validation->run() === FALSE)
		{
			$this->load->view('templates/header');
			$this->load->view('transaccion/nuevo',$data);
			$this->load->view('templates/footer');
		}
		else
		{
			$this->transaccionModel->crearTransaccion();
			redirect('transacciones', 'index');
		}
	}

	/**
	 * Permite al usuario ver datos del tipo de transaccion por id
	 * para poder editar los datos
	 * @param $id
	 * @return muestra la vista editar con datos del tipo de transaccion
	 */
	public function editar( $id = null)
	{

		$this->load->helper('form');
		$this->load->library('form_validation');

		$data['transaccion']= $this->transaccionModel->getTransaccion($id);

		$data['title'] = 'Editar Transacción';

		$this->load->view('templates/header');
		$this->load->view('transaccion/editar',$data);
		$this->load->view('templates/footer');

	}


	/**
	 * Modifica datos del tipo de transcción
	 * @param $id
	 * @return a la vista tipoTransaccion (lista de tipos de Transacciones )
	 */
	public function modificar($id)
	{
		$this->load->helper('form');
		$this->load->library('form_validation');
		$data['title'] = 'Editar Transacción';

		$this->form_validation->set_rules('id_cuenta_bancaria','Cuenta Bancaria','required');
		$this->form_validation->set_rules('id_tipo_transaccion','Tipo de transación','required');
		$this->form_validation->set_rules('monto','Monto','required');

		$data['transacciones'] = $this->transaccionModel->getTransacciones();

		if ($this->form_validation->run() === FALSE)
		{
			$this->load->view('templates/header');
			$this->load->view('transaccion/editar',$data);
			$this->load->view('templates/footer');
		}
		else
		{
			$this->transaccionModel->updateTransaccion($id);
			redirect('transacciones', 'index');
		}
	}

	/**
	 * Elimina registro del tipo de transacción con id = $id
	 * @param $id
	 * @return a la vista tipoTaransaccion (Lista de tipos de transacción)
	 */
	public function eliminar( $id = null)
	{
		$this->load->helper('form');
		$this->load->library('form_validation');

		$this->transaccionModel->deleteTransaccion($id);

		redirect('transacciones', 'index');

	}

}
