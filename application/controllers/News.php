<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News extends CI_Controller {

        public function __construct()
        {
                parent::__construct();
              
                $this->load->model('newModel');
                $this->load->helper('url_helper');
        }

        public function index()
        { 
            $data['news'] = $this->newModel->get_news();
            $data['title'] = 'News archive';
    
            $this->load->view('templates/header', $data);
            $this->load->view('news/index', $data);
            $this->load->view('templates/footer');
        }

        public function view($slug = NULL)
        {  
                $data['news_item'] = $this->newModel->get_news($slug);

                if (empty($data['news_item']))
                {
                        show_404();
                }

                $data['title'] = $data['news_item']['title'];

                $this->load->view('templates/header', $data);
                $this->load->view('news/view', $data);
                $this->load->view('templates/footer');
        }
}